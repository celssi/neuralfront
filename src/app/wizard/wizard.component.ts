import {ChangeDetectorRef, Component, ElementRef, OnDestroy, TemplateRef, ViewChild} from '@angular/core';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ImageObject} from '../image-object';
import {NflashService} from '../nflash.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {ImageCollection} from '../imageCollection';
import {NFlashMessage, NFlashMessageType} from '../n-flash-message';

@Component({
  selector: 'wizard',
  templateUrl: './wizard.component.html',
  styleUrls: ['./wizard.component.scss']
})
export class WizardComponent implements OnDestroy {

  private closeResult: string;

  images: ImageObject[] = [
    new ImageObject('http://neuralserver.celssi.ml/assets/images/kirurgi.jpg', 790, 424),
    new ImageObject('http://neuralserver.celssi.ml/assets/images/elephant.jpg', 800, 533),
    new ImageObject('http://neuralserver.celssi.ml/assets/images/books.jpg', 750, 469),
    new ImageObject('http://neuralserver.celssi.ml/assets/images/cats_and_dogs.jpg', 800, 800),
    new ImageObject('http://neuralserver.celssi.ml/assets/images/pizza.jpg', 832, 468),
    new ImageObject('http://neuralserver.celssi.ml/assets/images/tower.jpg', 750, 562),
    new ImageObject('http://neuralserver.celssi.ml/assets/images/umbrella.jpg', 780, 439)
  ];

  imageCollections: ImageCollection[] = [
    new ImageCollection('livingThings', 'http://neuralserver.celssi.ml/assets/imagecollections/livingThings.jpg', [
      new ImageObject('http://neuralserver.celssi.ml/assets/imagecollections/animals/1.jpeg', 527, 350),
      new ImageObject('http://neuralserver.celssi.ml/assets/imagecollections/animals/2.jpeg', 525, 350),
      new ImageObject('http://neuralserver.celssi.ml/assets/imagecollections/animals/3.jpeg', 525, 350),
      new ImageObject('http://neuralserver.celssi.ml/assets/imagecollections/animals/4.jpeg', 528, 350),
      new ImageObject('http://neuralserver.celssi.ml/assets/imagecollections/animals/5.jpg', 467, 350),
      new ImageObject('http://neuralserver.celssi.ml/assets/imagecollections/people/1.jpeg', 525, 350),
      new ImageObject('http://neuralserver.celssi.ml/assets/imagecollections/people/2.jpeg', 467, 350),
      new ImageObject('http://neuralserver.celssi.ml/assets/imagecollections/people/3.jpeg', 525, 350),
      new ImageObject('http://neuralserver.celssi.ml/assets/imagecollections/people/4.jpeg', 525, 350),
      new ImageObject('http://neuralserver.celssi.ml/assets/imagecollections/people/5.jpeg', 233, 350)
    ])
  ];

  imageQueue: ImageObject[] = [];
  wantedImages: ImageObject[] = [];
  notWantedImages: ImageObject[] = [];
  messages: NFlashMessage[] = [];

  step = 'home';
  showCollapseOne = false;
  showCollapseTwo = false;
  showCollapseThree = false;
  showResults = false;
  homeDisabled = false;
  sourceDisabled = true;
  conditionsDisabled = true;
  settingsDisabled = true;
  doneDisabled = true;
  selectedSource = null;
  selectedImage: ImageObject = null;
  selectedImageCollection: ImageCollection = null;
  selectedTarget = null;
  selectedFilter = null;
  canvasHeight = 0;
  queryId = null;
  currentImage = 0;
  prettyMessages = false;

  messagesSubscription = null;

  context: CanvasRenderingContext2D;

  @ViewChild('myCanvas')
  canvas: ElementRef;

  @ViewChild('imagemodal')
  imagemodal: TemplateRef<any>;

  @ViewChild('imagesmodal')
  imagesmodal: TemplateRef<any>;

  @ViewChild('textmodal')
  textmodal: TemplateRef<any>;

  constructor(private changeDetector: ChangeDetectorRef, private modalService: NgbModal, private nflashService: NflashService, private spinner: NgxSpinnerService) {
    this.messagesSubscription = nflashService.messages.subscribe((message: string) => {
      this.messages.push(new NFlashMessage(NFlashMessageType.Response, JSON.stringify(JSON.parse(message.indexOf('=') !== -1 ? message.split('=')[1] : message), null, 4)));
      console.log(this.messages[this.messages.length - 1]);

      if (message.indexOf('QUERY TEXT RESULT') === 0) {
        this.queryId = JSON.parse(message.split('=')[1]).queryId;
        this.sendMsg('SUBSCRIBE ' + this.queryId);
      } else if (message.indexOf('SUBSCRIBE RESULT') === 0) {
        this.sendMsg('START ' + this.queryId);
      } else if (message.indexOf('UNSUBSCRIBE RESULT') === 0) {

      } else if (message.indexOf('START RESULT') === 0) {
        this.sendCurrentImageToNFlash();
      } else if (message.indexOf('STOP RESULT') === 0) {
        this.sendMsg('UNSUBSCRIBE ' + this.queryId);
        this.queryId = null;
      } else {
        this.imageQueue[this.currentImage].imageResponse = JSON.parse(message.split('=')[1]);

        if ((this.selectedSource === 'image' || this.selectedSource === 'images') && (this.currentImage + 1) === this.imageQueue.length) {
          this.sendMsg('STOP ' + this.queryId);

          this.showResults = true;

          if (this.selectedSource === 'image') {
            this.changeDetector.detectChanges();

            const canvas = this.canvas.nativeElement;
            this.context = canvas.getContext('2d');

            this.canvasHeight = this.selectedImage.height * (this.context.canvas.width / this.selectedImage.width);

            this.drawImageAndBoundBoxes(this.selectedImage, this.context.canvas.width, this.canvasHeight);
          } else if (this.selectedSource === 'images') {
            for (let i = 0; i < this.imageQueue.length; i++) {
              const image = this.imageQueue[i];

              if (image.imageResponse != null && image.imageResponse.length > 0) {
                this.wantedImages.push(image);
              } else {
                this.notWantedImages.push(image);
              }
            }
          }

          this.spinner.hide();
          this.showCollapseTwo = true;
        } else if (this.selectedSource === 'images' && (this.currentImage + 1) < this.imageQueue.length) {
          this.currentImage = this.currentImage + 1;
          this.sendCurrentImageToNFlash();
        }
      }
    });
  }

  private static getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  static drawBoundBox(ctx, imageInfo, image, width, height) {
    ctx.strokeStyle = '#FF0000';
    ctx.strokeRect(imageInfo.bbox[1] * width, imageInfo.bbox[0] * height, imageInfo.bbox[3] * width - imageInfo.bbox[1] * width, imageInfo.bbox[2] * height - imageInfo.bbox[0] * height);
  }

  static drawScore(ctx, imageInfo, image, imageWidth, imageHeight) {
    ctx.fillStyle = 'white';
    ctx.font = '12px Arial';

    const text = (Math.round(imageInfo.score * 100) / 100).toString() + ' ' + imageInfo.object_class.name;
    const width = ctx.measureText(text).width;

    ctx.fillRect(imageInfo.bbox[1] * imageWidth + 1, imageInfo.bbox[0] * imageHeight + 1, width + 5, parseInt(ctx.font, 12));

    ctx.fillStyle = 'red';
    ctx.fillText(text, imageInfo.bbox[1] * imageWidth + 5, imageInfo.bbox[0] * imageHeight + 12);
  }

  static formatNQL(msg) {
    msg = msg.replace('INPUT ', 'INPUT\r\n\t');
    msg = msg.replace('QUERY TEXT ', 'QUERY TEXT\r\n\t');
    return msg;
  }

  setShow(value) {
    this.step = value;

    if (value === 'home') {
      this.homeDisabled = false;
      this.sourceDisabled = this.sourceDisabled && true;
      this.conditionsDisabled = this.conditionsDisabled && true;
      this.settingsDisabled = this.settingsDisabled && true;
      this.doneDisabled = true;
    } else if (value === 'source') {
      this.homeDisabled = this.homeDisabled && true;
      this.sourceDisabled = false;
      this.conditionsDisabled = this.conditionsDisabled && true;
      this.settingsDisabled = this.settingsDisabled && true;
      this.doneDisabled = true;
    } else if (value === 'conditions') {
      this.homeDisabled = this.homeDisabled && true;
      this.sourceDisabled = false;
      this.conditionsDisabled = false;
      this.settingsDisabled = this.settingsDisabled && true;
      this.doneDisabled = true;
    } else if (value === 'settings') {
      this.homeDisabled = this.homeDisabled && true;
      this.sourceDisabled = false;
      this.conditionsDisabled = false;
      this.settingsDisabled = false;
      this.doneDisabled = true;
    } else if (value === 'done') {
      window.scrollTo(0, 0);
      this.homeDisabled = true;
      this.sourceDisabled = true;
      this.conditionsDisabled = true;
      this.settingsDisabled = true;
      this.doneDisabled = false;
    }
  }

  toggleShowCollapseOne() {
    this.showCollapseOne = !this.showCollapseOne;
  }

  toggleShowCollapseTwo() {
    this.showCollapseTwo = !this.showCollapseTwo;
  }

  toggleShowCollapseThree() {
    this.showCollapseThree = !this.showCollapseThree;
  }

  setShowResults(show) {
    if (show) {
      this.spinner.show();

      if (this.selectedTarget === 'objects') {
        const queryMsg = 'QUERY TEXT From Vision.webcam_a As $wa Select $wa -> nm.obj.ssd_tensorflow.ssd_mobilenet_v1_coco_2017_11_17';

        if (this.selectedSource === 'image') {
          this.sendMsg(queryMsg);
        } else if (this.selectedSource === 'images') {
          this.sendMsg(queryMsg + ' ' + (this.selectedFilter === 'people' ? '{"include": ["person"]}' : '{"include": ["bird", "cat", "dog", "horse", "sheep", "cow", "elephant", "bear", "zebra", "giraffe" ]}'));
          this.currentImage = 0;
        }
      }
    } else {
      this.showResults = false;
    }
  }

  reset() {
    this.setShowResults(false);
    this.showCollapseOne = false;
    this.showCollapseTwo = false;
    this.showCollapseThree = false;
    this.selectedImage = null;
    this.selectedImageCollection = null;
    this.selectedSource = null;
    this.selectedTarget = null;
    this.queryId = null;
    this.selectedFilter = null;
    this.currentImage = 0;
    this.wantedImages = [];
    this.notWantedImages = [];
    this.messages = [];
    this.imageQueue = [];
    this.prettyMessages = false;
  }

  selectSource(source) {
    this.selectedSource = source;

    if (source === 'image') {
      this.openModal(this.imagemodal);
    } else if (source === 'images') {
      this.openModal(this.imagesmodal);
    } else if (source === 'text-file') {
      this.selectedImage = null;
      this.openModal(this.textmodal);
    }
  }

  selectImage(image) {
    this.selectedImage = this.images[image];
    this.imageQueue.push(this.selectedImage);
    this.selectedImageCollection = null;
    this.canvasHeight = this.selectedImage.height;
  }

  selectImageCollection(imageCollection) {
    this.selectedImageCollection = this.imageCollections[imageCollection];
    this.imageQueue = [].concat(this.selectedImageCollection.images);
    this.selectedImage = null;
  }

  selectTarget(target) {
    this.selectedTarget = target;
  }

  selectFilter(filter) {
    this.selectedFilter = filter;
  }

  allowNextOnSource() {
    return (this.selectedImage != null && this.selectedSource === 'image') || (this.selectedImageCollection != null && this.selectedSource === 'images');
  }

  allowNextOnFilter() {
    return (this.selectedSource === 'images' && this.selectedFilter) || this.selectedSource !== 'images';
  }

  sendMsg(msg) {
    this.messages.push(new NFlashMessage(NFlashMessageType.Request, WizardComponent.formatNQL(msg)));
    console.log(this.messages[this.messages.length - 1]);
    this.nflashService.sendMessage(msg);
  }

  sendCurrentImageToNFlash() {
    this.sendMsg('INPUT webcam_a {"image_url": "' + this.imageQueue[this.currentImage].url + '"}');
  }

  drawImageAndBoundBoxes(image, width, height) {
    const ctx = this.context;
    ctx.clearRect(0, 0, width, height);

    const img = new Image();
    img.src = this.selectedImage.url;
    img.addEventListener('load', () => {
      ctx.drawImage(img, 0, 0, width, height);

      const imageInfos = image.imageResponse;

      for (let i = 0; i < imageInfos.length; i++) {
        WizardComponent.drawBoundBox(ctx, imageInfos[i], image, width, height);
      }

      for (let i = 0; i < imageInfos.length; i++) {
        WizardComponent.drawScore(ctx, imageInfos[i], image, width, height);
      }
    }, false);
  }

  openModal(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      console.log(this.closeResult);
    }, (reason) => {
      this.closeResult = `Dismissed ${WizardComponent.getDismissReason(reason)}`;
    });
  }

  ngOnDestroy() {
    this.messagesSubscription.unsubscribe();
  }
}
