import { Component } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-contact-page',
  templateUrl: './contact-page.component.html',
  styleUrls: ['./contact-page.component.scss']
})
export class ContactPageComponent {

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  public model;
  public submitted = false;
  public errorMessage = '';

  constructor(private http: HttpClient) {
    this.reset();
  }

  onSubmit() {
    this.http.post('http://46.101.114.28:3000/contact', this.model, this.httpOptions).subscribe(
      data => {
        console.log('POST Request is successful ', data);
        this.reset();
        this.errorMessage = '';
        this.submitted = true;
      },
      error => {
        this.submitted = false;
        this.errorMessage = 'Something went terribly wrong :( Please try again later!';
      }
    );
  }

  reset() {
    this.model = {
      firstname: '',
      lastname: '',
      email: '',
      need: '',
      message: ''
    };
  }
}
