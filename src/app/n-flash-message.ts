export enum NFlashMessageType {
  Request,
  Response
}

export class NFlashMessage {
  type: NFlashMessageType;
  message: string;

  constructor(type: NFlashMessageType, message: string) {
    this.type = type;
    this.message = message;
  }

  isRequest() {
    return this.type === NFlashMessageType.Request;
  }

  isResponse() {
    return this.type === NFlashMessageType.Response;
  }

  asJson() {
    return JSON.parse(this.message);
  }

  inputAsJson() {
    return JSON.parse(this.message.split('webcam_a ')[1]);
  }

  isQueryText() {
    return this.message.indexOf('QUERY TEXT') === 0;
  }

  getQueryTextPretty() {
    return 'Sent request to identify ' + (this.message.indexOf('include') === -1 ? 'any objects' : this.message.indexOf('person') === -1 ? ' animals' : 'people') + ' from images to be sent soon';
  }

  getResponsePretty(target, source) {
    if (this.asJson().status) {
      let response = 'Server responded ';

      if (this.asJson().status === 'READY') {
        response += 'that it is ready';
      } else if (this.asJson().status === 'STARTED') {
        response += 'that it has started processing';
      } else if (this.asJson().status === 'STOPPED') {
        response += 'that it has stopped processing';
      } else if (this.asJson().status === 'Subscribed.') {
        response += 'that it has subscribed to query';
      } else if (this.asJson().status === 'Unsubscribed.') {
        response += 'that it has unsubsribed from query';
      }

      return response;
    } else if (target === 'objects' && this.asJson().length === 0 && source === 'images') {
      return 'Server responded that this image does not contain what it was asked to look for';
    } else if (target === 'objects' && this.asJson().length === 0) {
      return 'Server responded that it has no idea what was in the image';
    } else if (target === 'objects') {
      return 'Server responded that it found ' + this.asJson().length + ' object(s) from the image';
    }
  }

  isStart() {
    return this.message.indexOf('START') === 0;
  }

  getStartPretty() {
    return 'Asking server to start processing';
  }

  isStop() {
    return this.message.indexOf('STOP') === 0;
  }

  getStopPretty() {
    return 'Asking server to stop processing';
  }

  isInput() {
    return this.message.indexOf('INPUT') === 0;
  }

  getInputPretty() {
    return 'Asking server to process ' + (this.message.indexOf('image_url') > -1 ? ' an image:\r\n<img class="img-fluid img-thumbnail" src="' + this.inputAsJson().image_url + '" alt="" height="200" />' : '');
  }

  isSubscribe() {
    return this.message.indexOf('SUBSCRIBE') === 0;
  }

  getSubscribePretty() {
    return 'Subscribing to query';
  }

  isUnsubscribe() {
    return this.message.indexOf('UNSUBSCRIBE') === 0;
  }

  getUnsubscribePretty() {
    return 'Unsubscribing from query';
  }
}
