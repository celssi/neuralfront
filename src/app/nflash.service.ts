import { Injectable } from '@angular/core';
import { QueueingSubject } from 'queueing-subject';
import websocketConnect from 'rxjs-websockets';

@Injectable()
export class NflashService {
  input = new QueueingSubject<string>();
  messages = null;
  connectionStatus = null;
  connectionStatusSubscription = null;
  messagesSubscription = null;

  constructor() {
    this.openConnection();
  }

  sendMessage(message) {
    this.input.next(message);
  }

  openConnection() {
    const { messages, connectionStatus } = websocketConnect('ws://celssi.ml:9080', this.input);
    this.messages = messages;
    this.connectionStatus = connectionStatus;

    /*const connectionStatusSubscription = this.connectionStatus.subscribe(numberConnected => {
      console.log('number of connected websockets:', numberConnected);
    });*/

    /*const messagesSubscription = this.messages.subscribe((message: string) => {
      console.log('received message:', message);
    });*/
  }

  closeConnection() {
    this.messagesSubscription.unsubscribe();
    this.connectionStatusSubscription.unsubscribe();
  }
}
