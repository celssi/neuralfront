import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public navbarCollapsed = true;

  constructor(private translate: TranslateService, private cookieService: CookieService) {

  }

  static isAllowedLanguage(language) {
    return language === 'fi' || language === 'en';
  }

  ngOnInit() {
    const language = this.cookieService.get('language');
    const browserLang = this.translate.getBrowserLang();

    this.translate.addLangs(['en', 'fi']);

    AppComponent.isAllowedLanguage(language) ? this.translate.setDefaultLang(language) : this.translate.use(browserLang.match(/en|fi/) ? browserLang : 'en');
    AppComponent.isAllowedLanguage(language) ? this.cookieService.set('language', language) : this.cookieService.set('language', browserLang.match(/en|fi/) ? browserLang : 'en');
  }

  collapseNav() {
    this.navbarCollapsed = true;
  }

  changeLanguage(language) {
    if (AppComponent.isAllowedLanguage(language)) {
      this.cookieService.set('language', language);

      const cookieconsentStatus = this.cookieService.get('cookieconsent_status');

      if (cookieconsentStatus === 'dismiss') {
        this.translate.use(language);
      } else {
        window.location.reload();
      }
    }
  }
}
