import { TestBed, inject } from '@angular/core/testing';

import { NflashService } from './nflash.service';

describe('NflashService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NflashService]
    });
  });

  it('should be created', inject([NflashService], (service: NflashService) => {
    expect(service).toBeTruthy();
  }));
});
