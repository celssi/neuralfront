export class ImageObject {
  url: string;
  width: number;
  height: number;
  imageResponse: Array<any>;

  constructor(url: string, width: number, height: number) {
    this.url = url;
    this.width = width;
    this.height = height;
  }
}
