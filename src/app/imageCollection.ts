import {ImageObject} from './image-object';

export class ImageCollection {
  name: string;
  coverImage: string;
  images: ImageObject[];

  constructor(name: string, coverImage: string, images: ImageObject[]) {
    this.name = name;
    this.coverImage = coverImage;
    this.images = images;
  }
}
